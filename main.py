import xml.etree.ElementTree as etree
import codecs
import csv
import time
import os
import psycopg2
from psycopg2.extras import execute_values
import re
import string
import sys

PATH_BGT = 'E:\\NEDERLAND'

PG_USER = 'postgres'
PG_PASSWORD = 'password'
PG_DB = 'database'
PG_SCHEMA = 'new_schema'
PG_HOST = 'localhost'
PG_PORT = '5432'

BATCH_SIZE = 2**14
MAX_BATCHES = 20

# Nicely formatted time string
def hms_string(sec_elapsed):
    h = int(sec_elapsed / (60 * 60))
    m = int((sec_elapsed % (60 * 60)) / 60)
    s = sec_elapsed % 60
    return "{}:{:>02}:{:>05.2f}".format(h, m, s)


def strip_tag_name(t):
    idx = t.rfind("}")
    if idx != -1:
        t = t[idx + 1:]
    return t

conn = psycopg2.connect(dbname=PG_DB, user=PG_USER, password=PG_PASSWORD, host=PG_HOST, port = PG_PORT)
cur = conn.cursor()

gml = "{http://www.opengis.net/gml}"
imgeo = "{http://www.geostandaarden.nl/imgeo/2.1}"
xmlns = "{http://www.opengis.net/citygml/2.0}"

insert_start = 'INSERT INTO ' + PG_SCHEMA + '.'

end_string = imgeo + 'eindRegistratie'
id_string = gml + 'id',
height_string = imgeo + 'relatieveHoogteligging'

bgt_types = [
    {
        'name': 'tunneldeel',
        'parent': 'TunnelPart',
        'geometrie': imgeo + 'geometrie2dTunneldeel',
        'attribs': {},
        'insertstring': insert_start + 'tunneldeel (gml_id, relatievehoogteligging, geometrie) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s))'
    },
    {
        'name': 'overbruggingsdeel',
        'parent': 'BridgeConstructionElement',
        'geometrie': imgeo + 'geometrie2dOverbruggingsdeel',
        'attribs': {},
        'insertstring': insert_start + 'overbruggingsdeel (gml_id, relatievehoogteligging, geometrie) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s))'
    },
    {
        'name': 'kunstwerkdeel',
        'parent': 'Kunstwerkdeel',
        'geometrie': imgeo + 'geometrie2dOverigeConstructie',
        'attribs': {},
        'insertstring': insert_start + 'kunstwerkdeel (gml_id, relatievehoogteligging, geometrie) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s))'
    },
    {
        'name': 'scheiding',
        'parent': 'Scheiding',
        'geometrie': imgeo + 'geometrie2dOverigeConstructie',
        'attribs': {},
        'insertstring': insert_start + 'scheiding (gml_id, relatievehoogteligging, geometrie) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s))'
    },
    {
        'name': 'ondersteunendwaterdeel',
        'parent': 'OndersteunendWaterdeel',
        'geometrie': imgeo + 'geometrie2dOndersteunendWaterdeel',
        'attribs': {},
        'insertstring': insert_start + 'ondersteunendwaterdeel (gml_id, relatievehoogteligging, geometrie) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s))'
    },
    {
        'name': 'waterdeel',
        'parent': 'Waterdeel',
        'geometrie': imgeo + 'geometrie2dWaterdeel',
        'attribs': {},
        'insertstring': insert_start + 'waterdeel (gml_id, relatievehoogteligging, geometrie) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s))'
    },
    {
        'name': 'ondersteunendwegdeel',
        'parent': 'AuxiliaryTrafficArea',
        'geometrie': imgeo + 'geometrie2dOndersteunendWegdeel',
        'attribs': {
            'bgt_fysiekvoorkomen': imgeo + 'plus-fysiekVoorkomenOndersteunendWegdeel',
        },
        'insertstring': insert_start + 'ondersteunendwegdeel (gml_id, relatievehoogteligging, geometrie, bgt_fysiekvoorkomen) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s), %s)'
    },
    {
        'name': 'vegetatieobject',
        'parent': 'SolitaryVegetationObject',
        'geometrie': imgeo + 'geometrie2dVegetatieObject',
        'attribs': {
            'plus_type': imgeo + 'plus-type',
        },
        'insertstring': insert_start + 'vegetatieobject (gml_id, relatievehoogteligging, geometrie, plus_type) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s), %s)'
    },
    {
        'name': 'onbegroeidterreindeel',
        'parent': 'OnbegroeidTerreindeel',
        'geometrie': imgeo + 'geometrie2dOnbegroeidTerreindeel',
        'attribs': {
            'bgt_fysiekvoorkomen': imgeo + 'bgt-fysiekVoorkomen',
            'plus_fysiekvoorkomen': imgeo + 'plus-fysiekVoorkomen',
        },
        'insertstring': insert_start + 'onbegroeidterreindeel (gml_id, relatievehoogteligging, geometrie, bgt_fysiekvoorkomen, plus_fysiekvoorkomen) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s), %s, %s)'
    },
    {
        'name': 'begroeidterreindeel',
        'parent': 'PlantCover',
        'geometrie': imgeo + 'geometrie2dBegroeidTerreindeel',
        'attribs': {
            'bgt_fysiekvoorkomen': '{http://www.opengis.net/citygml/vegetation/2.0}class',
            'plus_fysiekvoorkomen': imgeo + 'plus-fysiekVoorkomen',
        },
        'insertstring': insert_start + 'begroeidterreindeel (gml_id, relatievehoogteligging, geometrie, bgt_fysiekvoorkomen, plus_fysiekvoorkomen) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s), %s, %s)'
    },
    {
        'name': 'pand',
        'parent': 'BuildingPart',
        'geometrie': imgeo + 'geometrie2dGrondvlak',
        'attribs': {},
        'insertstring': insert_start + 'pand (gml_id, relatievehoogteligging, geometrie) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s))'
    },
    {
        'name': 'wegdeel',
        'parent': 'TrafficArea',
        'geometrie': imgeo + 'geometrie2dWegdeel',
        'attribs': {
            'bgt_functie': imgeo + 'plus-functieWegdeel',
            'plus-fysiekvoorkomen': imgeo + 'plus-fysiekVoorkomenWegdeel'
        },
        'insertstring': insert_start + 'wegdeel (gml_id, relatievehoogteligging, geometrie, bgt_functie, plus_fysiekvoorkomen) VALUES %s',
        'template': '(%s, %s, ST_GeomFromGML(%s), %s, %s)'
    },
    
]

def get_polygon(geometry):
    exterior_string = ''
    interior_strings = []
    exterior = geometry.find(gml + 'exterior')
    if exterior.find(gml + 'LinearRing') is not None:
        exterior_string = etree.tostring(exterior, encoding='unicode')
    else:
        segments = exterior[0][0][0][0]
        segment_strings = []
        segment_strings.append(segments[0][0].text)
        for seg in segments[1:]:
            segment_strings.append(' '.join(seg[0].text.split(' ')[2:]))
        exterior_string = '<ns0:exterior xmlns:ns0="http://www.opengis.net/gml"><ns0:LinearRing><ns0:posList srsDimension="2">' + ' '.join(segment_strings) + '</ns0:posList></ns0:LinearRing></ns0:exterior>'

    interiors = geometry.findall(gml + 'interior')
    for interior in interiors:
        if interior.find(gml + 'LinearRing') is not None:
            interior_strings.append(etree.tostring(interior, encoding='unicode'))
        else: 
            segments = interior[0][0][0][0]
            segment_strings = []
            segment_strings.append(segments[0][0].text)
            for seg in segments[1:]:
                segment_strings.append(' '.join(seg[0].text.split(' ')[2:]))
            interior_strings.append('<ns0:interior xmlns:ns0="http://www.opengis.net/gml"><ns0:LinearRing><ns0:posList srsDimension="2">' + ' '.join(segment_strings) + '</ns0:posList></ns0:LinearRing></ns0:interior>')

    obj['geometrie'] = '<ns0:Polygon xmlns:ns0="http://www.opengis.net/gml" srsName="urn:ogc:def:crs:EPSG::28992">' + exterior_string + ''.join(interior_strings) + '</ns0:Polygon>'
    return '<ns0:Polygon xmlns:ns0="http://www.opengis.net/gml" srsName="urn:ogc:def:crs:EPSG::28992">' + exterior_string + ''.join(interior_strings) + '</ns0:Polygon>'

def get_multisurface(geometry):
    surface_members = geometry.findall(gml + 'surfaceMember')
    sm_strings = []
    for sm in surface_members:
        if strip_tag_name(sm[0].tag) == 'Polygon':
            sm_strings.append(get_polygon(sm[0]))
        else:
            print(etree.tostring(geometry, encoding='unicode'))
            raise Exception('This shouldn\'t happen')
    return '<ns0:MultiSurface xmlns:ns0="http://www.opengis.net/gml"><ns0:surfaceMember>' + ''.join(sm_strings) + '</ns0:surfaceMember></ns0:MultiSurface>'

def get_point(geometry):
    return etree.tostring(geometry, encoding='unicode')

start_time = time.time()

bgt_work = bgt_types

if sys.argv[1] is not None:
    try:
        start = int(sys.argv[1])
        end = int(sys.argv[2])
        print('Executing from', str(start), 'to', str(end) )
        bgt_work = bgt_types[start:end]
    except:
        start = int(sys.argv[1])
        print('Executing from', str(start), 'to', str(start + 1))
        bgt_work = bgt_types[start:start+1]

# Do stuff
for bgt in bgt_work:
    path_to_file = os.path.join(PATH_BGT, 'bgt_' + bgt['name']) + '.gml'

    # get an iterable
    context = etree.iterparse(path_to_file, events=("start", "end"))

    # Init insert counter
    counter = 0
    data = []
    totalCount = 0
    previous_time = time.time()
    skipped = 0
    batch = 0

    print('Processing', bgt['name'])

    for event, element in context:
        #if event == 'start':
        #    pass
        if event == 'end' and strip_tag_name(element.tag) == bgt['parent']:
            #tname = strip_tag_name(element.tag)
            #if tname == bgt['parent']:

                obj = {}
                obj['gml_id'] = element.attrib['{http://www.opengis.net/gml}id']

                # Skip if eindregistratie exists
                if element.find(end_string) is not None:
                    skipped = skipped + 1
                    element.clear()
                    continue
                obj['relatievehoogteligging'] = int(element.find("{http://www.geostandaarden.nl/imgeo/2.1}relatieveHoogteligging").text)
                geometry = element.find(bgt['geometrie'])[0]

                # Process polygon
                if strip_tag_name(geometry.tag) == 'LineString':
                    skipped = skipped + 1
                    element.clear()
                    continue
                elif strip_tag_name(geometry.tag) == 'Polygon':
                    obj['geometrie'] = get_polygon(geometry)
                elif strip_tag_name(geometry.tag) == 'Point':
                    obj['geometrie'] = get_point(geometry)
                elif strip_tag_name(geometry.tag) == 'MultiSurface':
                    obj['geometrie'] = get_multisurface(geometry)
                elif strip_tag_name(geometry.tag) == 'Curve':
                    skipped = skipped + 1
                    element.clear()
                    continue

                # Skip multipoints because they're of no use to us
                elif strip_tag_name(geometry.tag) == 'MultiPoint':
                    skipped = skipped + 1
                    element.clear()
                    continue
                for key, value in bgt['attribs'].items():
                    obj[key] = element.find(value).text

                args = list(obj.values())
                data.append(args)

                counter = counter + 1

                if counter == BATCH_SIZE:
                    batch = batch + 1
                    try:
                        execute_values(cur, bgt['insertstring'], data, template=bgt['template'], page_size=BATCH_SIZE)
                    except:
                        print("Unexpected error:", sys.exc_info()[0])
                    data = []
                    totalCount = totalCount + counter
                    if BATCH_SIZE > 1:
                        print('Batch', str(batch) + ', total:', totalCount)
                    counter = 0
                    if batch % MAX_BATCHES == 0:
                        print('Committing batch')
                        conn.commit()
                element.clear()
    
    if counter > 0:
        try:
            execute_values(cur, bgt['insertstring'], data, template=bgt['template'], page_size=BATCH_SIZE)
        except:
            print("Unexpected error:", sys.exc_info()[0])
        data = []
        totalCount = totalCount + counter
        if BATCH_SIZE > 1:
            print('Batch', str(batch) + ', total:', totalCount)  
        counter = 0

    time_took = time.time() - previous_time
    print(f"Runtime: {hms_string(time_took)}")
    print('Total:', totalCount)
    conn.commit()

conn.close()

time_took = time.time() - start_time
print(f"Total runtime: {hms_string(time_took)}")