ST_CurveToLine()
 {
         GSERIALIZED *geom = PG_GETARG_GSERIALIZED_P(0);
         double tol = PG_GETARG_FLOAT8(1);
         int toltype = PG_GETARG_INT32(2);
         int flags = PG_GETARG_INT32(3);
         GSERIALIZED *ret;
         LWGEOM *igeom = NULL, *ogeom = NULL;
 
         POSTGIS_DEBUG(2, "ST_CurveToLine called.");
 
         POSTGIS_DEBUGF(3, "tol = %g, typ = %d, flg = %d", tol, toltype, flags);
 
         igeom = lwgeom_from_gserialized(geom);
         ogeom = lwcurve_linearize(igeom, tol, toltype, flags);
         lwgeom_free(igeom);
 
         if (ogeom == NULL)
                 PG_RETURN_NULL();
 
         ret = geometry_serialize(ogeom);
         lwgeom_free(ogeom);
         PG_FREE_IF_COPY(geom, 0);
         PG_RETURN_POINTER(ret);
 }

lwcurve_linearize()
 {
         LWGEOM * ogeom = NULL;
         switch (geom->type)
         {
         case CIRCSTRINGTYPE:
                 ogeom = (LWGEOM *)lwcircstring_linearize((LWCIRCSTRING *)geom, tol, type, flags);
                 break;
         case COMPOUNDTYPE:
                 ogeom = (LWGEOM *)lwcompound_linearize((LWCOMPOUND *)geom, tol, type, flags);
                 break;
         case CURVEPOLYTYPE:
                 ogeom = (LWGEOM *)lwcurvepoly_linearize((LWCURVEPOLY *)geom, tol, type, flags);
                 break;
         case MULTICURVETYPE:
                 ogeom = (LWGEOM *)lwmcurve_linearize((LWMCURVE *)geom, tol, type, flags);
                 break;
         case MULTISURFACETYPE:
                 ogeom = (LWGEOM *)lwmsurface_linearize((LWMSURFACE *)geom, tol, type, flags);
                 break;
         case COLLECTIONTYPE:
                 ogeom = (LWGEOM *)lwcollection_linearize((LWCOLLECTION *)geom, tol, type, flags);
                 break;
         default:
                 ogeom = lwgeom_clone(geom);
         }
         return ogeom;
 }