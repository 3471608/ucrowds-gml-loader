CREATE TABLE begroeidterreindeel AS SELECT gml_id, relatievehoogteligging, bgt_fysiekvoorkomen, plus_fysiekvoorkomen, ST_CurveToLine(geometrie_vlak) FROM latest.begroeidterreindeel WHERE eindregistratie IS NULL;
ALTER TABLE public.begroeidterreindeel RENAME st_curvetoline TO geometrie;
CREATE INDEX geom_idx_begroeidterreindeel_geometrie ON public.begroeidterreindeel USING gist (geometrie);
ANALYZE begroeidterreindeel;

CREATE TABLE pand AS SELECT gml_id, relatievehoogteligging, ST_CurveToLine(geometrie_vlak) from latest.pand WHERE eindregistratie IS NULL;
CREATE INDEX geom_idx_pand_geometrie ON public.pand USING gist (geometrie);
ANALYZE pand;

CREATE TABLE wegdeel AS SELECT gml_id, relatievehoogteligging, ST_CurveToLine(geometrie_vlak) FROM latest.wegdeel WHERE eindregistratie IS NULL;
CREATE INDEX geom_idx_wegdeel_geometrie ON public.wegdeel USING gist (geometrie);
ANALYZE wegdeel;

CREATE TABLE onbegroeidterreindeel AS SELECT gml_id, relatievehoogteligging, bgt_fysiekvoorkomen, plus_fysiekvoorkomen, ST_CurveToLine(geometrie_vlak) FROM latest.onbegroeidterreindeel;

CREATE INDEX geom_idx_onbegroeidterreindeel_geometrie ON public.onbegroeidterreindeel USING gist (geometrie);
ANALYZE onbegroeidterreindeel;

CREATE TABLE begroeidterreindeel AS SELECT gml_id, relatievehoogteligging, bgt_fysiekvoorkomen, plus_fysiekvoorkomen, ST_CurveToLine(geometrie_vlak) FROM latest.begroeidterreindeel WHERE eindregistratie IS NULL;
ALTER TABLE public.begroeidterreindeel RENAME st_curvetoline TO geometrie;
CREATE INDEX geom_idx_begroeidterreindeel_geometrie ON public.begroeidterreindeel USING gist (geometrie);
ANALYZE begroeidterreindeel;

CREATE TABLE overbruggingsdeel AS SELECT gml_id, relatievehoogteligging, typeoverbruggingsdeel, hoortbijtypeoverbrugging, ST_CurveToLine(geometrie_vlak) FROM latest.overbruggingsdeel WHERE eindregistratie IS NULL;
ALTER TABLE public.overbruggingsdeel RENAME st_curvetoline TO geometrie;
CREATE INDEX geom_idx_overbruggingsdeel_geometrie ON public.overbruggingsdeel USING gist (geometrie);
ANALYZE overbruggingsdeel;

-- tunneldeel
CREATE TABLE tunneldeel AS SELECT gml_id, relatievehoogteligging, ST_CurveToLine(geometrie_vlak) FROM latest.tunneldeel WHERE eindregistratie IS NULL;
ALTER TABLE public.tunneldeel RENAME st_curvetoline TO geometrie;
CREATE INDEX geom_idx_tunneldeel_geometrie ON public.tunneldeel USING gist (geometrie);
ANALYZE tunneldeel;

-- waterdeel
CREATE TABLE waterdeel AS SELECT gml_id, relatievehoogteligging, ST_CurveToLine(geometrie_vlak, 8, 0, 1) FROM latest.waterdeel WHERE eindregistratie IS NULL;
ALTER TABLE public.waterdeel RENAME st_curvetoline TO geometrie;
CREATE INDEX geom_idx_waterdeel_geometrie ON public.waterdeel USING gist (geometrie);
ANALYZE waterdeel;
