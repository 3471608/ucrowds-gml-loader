CREATE SCHEMA new_schema
GRANT ALL ON SCHEMA new_schema TO postgres;
ALTER DEFAULT PRIVILEGES IN SCHEMA new_schema GRANT ALL PRIVILEGES ON TABLES TO postgres;

-- Table: new_schema.begroeidterreindeel

-- DROP TABLE new_schema.begroeidterreindeel;

CREATE UNLOGGED TABLE new_schema.begroeidterreindeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    bgt_fysiekvoorkomen character varying COLLATE pg_catalog."default",
    plus_fysiekvoorkomen character varying COLLATE pg_catalog."default",
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.begroeidterreindeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.begroeidterreindeel TO postgres;

-- Table: new_schema.kunstwerkdeel

-- DROP TABLE new_schema.kunstwerkdeel;

CREATE UNLOGGED TABLE new_schema.kunstwerkdeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.kunstwerkdeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.kunstwerkdeel TO postgres;

-- Table: new_schema.onbegroeidterreindeel

-- DROP TABLE new_schema.onbegroeidterreindeel;

CREATE UNLOGGED TABLE new_schema.onbegroeidterreindeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    bgt_fysiekvoorkomen character varying(255) COLLATE pg_catalog."default",
    plus_fysiekvoorkomen character varying(255) COLLATE pg_catalog."default",
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.onbegroeidterreindeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.onbegroeidterreindeel TO postgres;

-- Table: new_schema.ondersteunendwaterdeel

-- DROP TABLE new_schema.ondersteunendwaterdeel;

CREATE UNLOGGED TABLE new_schema.ondersteunendwaterdeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.ondersteunendwaterdeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.ondersteunendwaterdeel TO postgres;

-- Table: new_schema.ondersteunendwegdeel

-- DROP TABLE new_schema.ondersteunendwegdeel;

CREATE UNLOGGED TABLE new_schema.ondersteunendwegdeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    bgt_fysiekvoorkomen character varying(255) COLLATE pg_catalog."default",
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.ondersteunendwegdeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.ondersteunendwegdeel TO postgres;

-- Table: new_schema.overbruggingsdeel

-- DROP TABLE new_schema.overbruggingsdeel;

CREATE UNLOGGED TABLE new_schema.overbruggingsdeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    typeoverbruggingsdeel character varying(255) COLLATE pg_catalog."default",
    hoortbijtypeoverbrugging character varying(255) COLLATE pg_catalog."default",
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.overbruggingsdeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.overbruggingsdeel TO postgres;

-- Table: new_schema.paal

-- DROP TABLE new_schema.paal;

CREATE UNLOGGED TABLE new_schema.paal
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.paal
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.paal TO postgres;

-- Table: new_schema.pand

-- DROP TABLE new_schema.pand;

CREATE UNLOGGED TABLE new_schema.pand
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.pand
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.pand TO postgres;

-- Table: new_schema.scheiding

-- DROP TABLE new_schema.scheiding;

CREATE UNLOGGED TABLE new_schema.scheiding
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.scheiding
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.scheiding TO postgres;

-- Table: new_schema.tunneldeel

-- DROP TABLE new_schema.tunneldeel;

CREATE UNLOGGED TABLE new_schema.tunneldeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.tunneldeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.tunneldeel TO postgres;

-- Table: new_schema.vegetatieobject

-- DROP TABLE new_schema.vegetatieobject;

CREATE UNLOGGED TABLE new_schema.vegetatieobject
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    plus_type character varying(255) COLLATE pg_catalog."default",
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.vegetatieobject
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.vegetatieobject TO postgres;

-- Table: new_schema.waterdeel

-- DROP TABLE new_schema.waterdeel;

CREATE UNLOGGED TABLE new_schema.waterdeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.waterdeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.waterdeel TO postgres;

-- Table: new_schema.wegdeel

-- DROP TABLE new_schema.wegdeel;

CREATE UNLOGGED TABLE new_schema.wegdeel
(
    gml_id character varying COLLATE pg_catalog."default",
    relatievehoogteligging integer,
    bgt_functie character varying(255) COLLATE pg_catalog."default",
    plus_fysiekvoorkomen character varying(255) COLLATE pg_catalog."default",
    geometrie geometry
)

TABLESPACE pg_default;

ALTER TABLE new_schema.wegdeel
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.wegdeel TO postgres;

-- Table: new_schema.connection

-- DROP TABLE new_schema.connection;

CREATE UNLOGGED TABLE new_schema.connection
(
    id_a character varying COLLATE pg_catalog."default",
    id_b character varying COLLATE pg_catalog."default",
    index_a integer,
    edge geometry,
    layer_a integer,
    layer_b integer
)

TABLESPACE pg_default;

ALTER TABLE new_schema.connection
    OWNER to postgres;

GRANT ALL ON TABLE new_schema.connection TO postgres;