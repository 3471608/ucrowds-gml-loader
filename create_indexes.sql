-- Index: idx_begroeidterreindeel_geometrie

-- DROP INDEX new_schema.idx_begroeidterreindeel_geometrie;

CREATE INDEX idx_begroeidterreindeel_geometrie
    ON new_schema.begroeidterreindeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.begroeidterreindeel SET LOGGED;


-- Index: idx_begroeidterreindeel_gml_id

-- DROP INDEX new_schema.idx_begroeidterreindeel_gml_id;

CREATE INDEX idx_begroeidterreindeel_gml_id
    ON new_schema.begroeidterreindeel USING btree
    (gml_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: idx_kunstwerkdeel_geometrie

-- DROP INDEX new_schema.idx_kunstwerkdeel_geometrie;

CREATE INDEX idx_kunstwerkdeel_geometrie
    ON new_schema.kunstwerkdeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.kunstwerkdeel SET LOGGED;

-- Index: idx_onbegroeidterreindeel_geometrie

-- DROP INDEX new_schema.idx_onbegroeidterreindeel_geometrie;

CREATE INDEX idx_onbegroeidterreindeel_geometrie
    ON new_schema.onbegroeidterreindeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.onbegroeidterreindeel SET LOGGED;

-- Index: idx_onbegroeidtereindeel_gml_id

-- DROP INDEX new_schema.idx_onbegroeidtereindeel_gml_id;

CREATE INDEX idx_onbegroeidtereindeel_gml_id
    ON new_schema.onbegroeidterreindeel USING btree
    (gml_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;

-- Index: idx_ondersteunendwaterdeel_geometrie

-- DROP INDEX new_schema.idx_ondersteunendwaterdeel_geometrie;

CREATE INDEX idx_ondersteunendwaterdeel_geometrie
    ON new_schema.ondersteunendwaterdeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.ondersteunendwaterdeel SET LOGGED;

-- Index: idx_ondersteunendwegdeel_geometrie

-- DROP INDEX new_schema.idx_ondersteunendwegdeel_geometrie;

CREATE INDEX idx_ondersteunendwegdeel_geometrie
    ON new_schema.ondersteunendwegdeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.ondersteunendwegdeel SET LOGGED;

-- Index: idx_overbruggingsdeel_geometrie

-- DROP INDEX new_schema.idx_overbruggingsdeel_geometrie;

CREATE INDEX idx_overbruggingsdeel_geometrie
    ON new_schema.overbruggingsdeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.overbruggingsdeel SET LOGGED;

-- Index: idx_paal_geometrie

-- DROP INDEX new_schema.idx_paal_geometrie;

CREATE INDEX idx_paal_geometrie
    ON new_schema.paal USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.paal SET LOGGED;
    
-- Index: idx_pand_geometrie

-- DROP INDEX new_schema.idx_pand_geometrie;

CREATE INDEX idx_pand_geometrie
    ON new_schema.pand USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.pand SET LOGGED;
    
-- Index: idx_scheiding_geometrie

-- DROP INDEX new_schema.idx_scheiding_geometrie;

CREATE INDEX idx_scheiding_geometrie
    ON new_schema.scheiding USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.scheiding SET LOGGED;


-- Index: idx_scheiding_gml_id

-- DROP INDEX new_schema.idx_scheiding_gml_id;

CREATE INDEX idx_scheiding_gml_id
    ON new_schema.scheiding USING btree
    (gml_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
    
-- Index: idx_tunneldeel_geometrie

-- DROP INDEX new_schema.idx_tunneldeel_geometrie;

CREATE INDEX idx_tunneldeel_geometrie
    ON new_schema.tunneldeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.tunneldeel SET LOGGED;
    
-- Index: idx_vegetatieobject_geometrie

-- DROP INDEX new_schema.idx_vegetatieobject_geometrie;

CREATE INDEX idx_vegetatieobject_geometrie
    ON new_schema.vegetatieobject USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.vegetatieobject SET LOGGED;
    
-- Index: idx_waterdeel_geometrie

-- DROP INDEX new_schema.idx_waterdeel_geometrie;

CREATE INDEX idx_waterdeel_geometrie
    ON new_schema.waterdeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.waterdeel SET LOGGED;
    
-- Index: idx_wegdeel_geometrie

-- DROP INDEX new_schema.idx_wegdeel_geometrie;

CREATE INDEX idx_wegdeel_geometrie
    ON new_schema.wegdeel USING gist
    (geometrie)
    TABLESPACE pg_default;
ALTER TABLE new_schema.wegdeel SET LOGGED;


-- Index: idx_wegdeel_id

-- DROP INDEX new_schema.idx_wegdeel_id;

CREATE INDEX idx_wegdeel_id
    ON new_schema.wegdeel USING btree
    (gml_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;